﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
	lens - a simple command-line app to pull max column length on every column in a CSV
	because fuck these truncation errors, seriously, christ alive
*/

namespace lens
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args[0] == "?")
			{
				Console.WriteLine("lens - a simple command-line app to pull max column length on every column in a CSV");
				Console.WriteLine("USAGE:");
				Console.WriteLine("lens filename.txt |");
				Console.WriteLine(">>> Processes a file called filename.txt with a delimiter character |, with headers");
				Console.WriteLine();
				Console.WriteLine("lens filename.txt | noheaders");
				Console.WriteLine(">>> Processes a file called filename.txt with a delimiter character |, with no headers");
				Console.WriteLine("lens filename.txt | colasc");
				Console.WriteLine(">>> Processes a file called filename.txt with a delimiter character |, with headers, output column names ascending");
				Console.WriteLine();
				Console.WriteLine("lens filename.txt | noheaders colasc");
				Console.WriteLine(">>> Processes a file called filename.txt with a delimiter character |, with no headers, output column names ascending");

				return;
			}

			string fileName = Path.Combine(Directory.GetCurrentDirectory(), args[0]);
			char delimiter = args[1][0];
			bool hasHeaders = true;
			bool colAsc = false;

			hasHeaders = args.Contains("noheaders") ? false : true;
			colAsc = args.Contains("colasc") ? true : false;

			List<Tuple<string, int, string>> columnLengths = new List<Tuple<string, int, string>>();
			string[] columnNames = { };
			string line = null;

			Console.WriteLine(string.Format("Analyzing {0}...\r\n", fileName));
			using (StreamReader sr = new StreamReader(fileName))
			{
				if (hasHeaders)
				{
					columnNames = sr.ReadLine().Split(delimiter);
					line = sr.ReadLine();
				}
				else
				{
					// becomes Column0, Column1, Column2, etc.
					line = sr.ReadLine();
					columnNames = Enumerable.Range(0, line.Split(delimiter).Count()).Select(x => "Column" + x.ToString()).ToArray();
				}

				do
				{
					string[] lineColumns = line.Split(delimiter);
					for (int i = 0; i < columnNames.Length; i++)
					{
						columnLengths.Add(new Tuple<string, int, string>(columnNames[i], lineColumns[i].Length, lineColumns[i]));
					}

					line = sr.ReadLine();
				} while (line != null);

				var maxLengths = columnNames.Select(x => columnLengths.Where(l => l.Item1 == x).OrderByDescending(l => l.Item2).First());
				var columnNamesIndexed = columnNames.Select((x, i) => new KeyValuePair<string, int>(x, i)).OrderBy(x => x.Value).ToList();

				var colAscList = (
					from Item1 in maxLengths
					join Item2 in columnNamesIndexed
					on Item1.Item1 equals Item2.Key
					select new { Item1, Item2.Value }
				).ToList();

				// refactor this awful mess
				foreach (var q in colAscList.OrderByDescending(x => colAsc == true ? x.Value * -1 : x.Item1.Item2))
				{
					Console.WriteLine(string.Format("{0}:\t{1}\tData: {2}", q.Item1.Item1, q.Item1.Item2, q.Item1.Item3));
				}
			}
		}
	}
}
